#include "Locadora.h"

void L_Upload(void){
  int i;
  char linha[5], *token, txt[256];
  FILE *arquivo;
  arquivo = fopen("DB.txt","r");
  if(arquivo==NULL){
  	perror("\nErro ao tentar ler o banco de dados!");
  	getchar();
  	exit(0);}

  else{
  	fgets(linha, (sizeof(linha)-1), arquivo);
  	cont = atoi(linha);
    p = (struct Locadora *) malloc (cont*sizeof(struct Locadora));
    for(i=0;i<cont;i++){
  		fgets(txt, (sizeof(txt)-1), arquivo);
  		p[i].codigo=i+1;
  		token = strtok(txt, ";");
  		strcpy(p[i].titulo,token);
  		token = strtok(NULL, ";");
  		p[i].ano = atoi(token);
  		token = strtok(NULL, ";");
  		p[i].quantidade = atoi(token);
      p[i].qnto=p[i].quantidade;
  		token = strtok(NULL, "\n");
  		strcpy(p[i].genero,token);}
      fclose(arquivo);}}

void L_Exit(void){
	char sn;
  system("clear");
  L_Header();
	printf("\nVoce está prestes a sair do programa...\nDigite 'S' para prosseguir ou 'n' para retornar ao menu: [S/n]\n\n-> " );
	scanf(" %c",&sn);
		switch(sn){
			case 'S':
      case 's':
			system("clear");
    	exit (0);
      break;

      case 'N':
      case 'n':
      system("clear");
      L_Header();
      L_Menu();

      default:
    printf("Opcao Invalida!\nPor favor digite 'S' para sair do programa ou 'n' para retornar ao menu...\n");
    char son;
    scanf(" %c",&son);
    if(son == 'S' || son == 's'){
      system("clear");
      exit (0);}
    else{
      system("clear");
      L_Header();
      L_Menu();}break;}}

void L_Header(void){
	printf("\n\n---------------------------------------------------------\n");
	printf("\t\t\tLOCADORA\n");
	printf("---------------------------------------------------------\n\n");}

void L_Menu(void){
	int menu;
	printf("\n\tNumero Total de Titulos Cadastrados: %i", cont);
	printf("\n\t\t\t  Menu\n1)Locacao\n2)Entrega\n3)Busca\n4)Relatorio\n0)Encerrar\n\n-> ");
	scanf("%d",&menu);
	switch(menu){
		case 0:
		L_Exit();
		break;

    case 1:
    L_Rent();
    break;

    case 2:
    L_GBack();
    break;

    case 3:
    L_Search();
    break;

    case 4:
    L_MkFile();
    break;}}

void L_Rent(void){
  int r,c,i;
  system("clear");
  L_Header();
  printf("\n0)Retornar\n1)Inserir Codigo\n\n-> ");
  scanf("%d",&r);
  switch(r){
    case 0:
    system("clear");
    L_Header();
    L_Menu();
    break;

    case 1:
    system("clear");
    L_Header();
    printf("\nCodigo: ");
    scanf(" %d",&c);
    for(i=0;i<cont;i++){
      if(p[i].codigo == c){
        if(p[i].quantidade>0){
        p[i].quantidade = p[i].quantidade-1;
        printf("\nBoa Escolha!\nO filme: %s foi alugado com sucesso!\n",p[i].titulo);
        printf("\n\n");
        L_Header();
        L_Menu();}
        else{
        printf("\nPuts :/\nInfelizmente nao há mais nenhum exemplar de %s disponivel em estoque.\nTente novamente dentro de alguns dias.",p[i].titulo);
        printf("\n\n");
        L_Header();
        L_Menu();
      }}}
      break;

      default:
      system("clear");
      L_Header();
      L_Search();
      break;}}

void L_GBack(void){
  int r,c,i;
  char l;
  system("clear");
  L_Header();
  printf("\n0)Retornar\n1)Inserir Codigo\n\n-> ");
  scanf("%d",&r);
  switch(r){
    case 0:
    system("clear");
    L_Header();
    L_Menu();
    break;

    case 1:
    system("clear");
    L_Header();
    printf("\nCodigo: ");
    scanf(" %d",&c);
    for(i=0;i<cont;i++){
      if(p[i].codigo == c){
        printf("O filme que deseja devolver e: %s? [S/n]\n",p[i].titulo);
        scanf(" %c",&l);
        switch(l){
        case 'S':
        case 's':
          if(p[i].quantidade<p[i].qnto){
            p[i].quantidade = p[i].quantidade+1;
            printf("\nAgradecemos a preferencia!\nO filme: %s foi devolvido com sucesso!\n",p[i].titulo);
            printf("\n\n");
            L_Header();
            L_Menu();}
          if(p[i].quantidade==p[i].qnto){
            printf("\nAgradecemos a preferencia!\nMas o filme: %s nao pôde ser devolvido por nao pertencer ao nosso acervo!\n",p[i].titulo);
            printf("\n\n");
            L_Header();
            L_Menu();}
          break;

        case 'N':
        case 'n':
        system("clear");
        L_Header();
        L_GBack();

        default:
        system("clear");
        L_Header();
        L_Search();
        break;}}}
      break;

      default:
      system("clear");
      L_Header();
      L_Search();
      break;}}

void L_Search(void){
  int i,r,c;
  char t[99];
  system("clear");
  L_Header();
  printf("\n\t\t       Buscar por:\n0)Retornar\n1)Codigo\n2)Titulo\n3)Ano\n4)Genero\n5)Todos\n\n-> ");
  scanf("%d",&r);
  switch(r){
  case 0:
    system("clear");
    L_Header();
    L_Menu();
    break;
  case 1:
      system("clear");
      L_Header();
      printf("\nCodigo: ");
      scanf(" %d",&c);
      for(i=0;i<cont;i++){
        if(p[i].codigo == c){
          printf("\n\t\t\t  Informacoes");
          printf("\nCodigo: %d\nTitulo: %s\nAno: %d\nQuantidade: %d\nGenero: %s\n",(p+i)->codigo,(p+i)->titulo,(p+i)->ano,(p+i)->quantidade,(p+i)->genero);
          printf("\n");
          }}
          L_Header();
          L_Menu();
          break;
    case 2:
        system("clear");
        L_Header();
        printf("\nTitulo: ");
        scanf(" %[^\n]s", t);
        for(i=0;i<cont;i++){
          if(strcmp(p[i].titulo,t)==0){
            printf("\n\t\t\t  Informacoes");
            printf("\nCodigo: %d\nTitulo: %s\nAno: %d\nQuantidade: %d\nGenero: %s\n",(p+i)->codigo,(p+i)->titulo,(p+i)->ano,(p+i)->quantidade,(p+i)->genero);
            printf("\n");
            }}
            L_Header();
            L_Menu();
            break;
      case 3:
          system("clear");
          L_Header();
          printf("\nAno: ");
          scanf(" %d",&c);
          for(i=0;i<cont;i++){
            if(p[i].ano == c){
              printf("\n\t\t\t  Informacoes");
              printf("\nCodigo: %d\nTitulo: %s\nAno: %d\nQuantidade: %d\nGenero: %s\n",(p+i)->codigo,(p+i)->titulo,(p+i)->ano,(p+i)->quantidade,(p+i)->genero);
              printf("\n");
              }}
              L_Header();
              L_Menu();
              break;
      case 4:
          system("clear");
          L_Header();
          printf("\nGenero: ");
          scanf(" %s", t);
          for(i=0;i<cont;i++){
            if(strcmp(p[i].genero,t)==0){
              printf("\n\t\t\t  Informacoes");
              printf("\nCodigo: %d\nTitulo: %s\nAno: %d\nQuantidade: %d\nGenero: %s\n",(p+i)->codigo,(p+i)->titulo,(p+i)->ano,(p+i)->quantidade,(p+i)->genero);
              printf("\n");
            }}
            L_Header();
            L_Menu();
            break;
        case 5:
          system("clear");
          L_Header();
          for(i=0;i<cont;i++){
          printf("\nCodigo: %d\nTitulo: %s\nAno: %d\nQuantidade: %d\nGenero: %s\n",(p+i)->codigo,(p+i)->titulo,(p+i)->ano,(p+i)->quantidade,(p+i)->genero);
          }
          printf("\n\n\n");
          L_Header();
          L_Menu();
          break;
        default:
            system("clear");
            L_Header();
            L_Search();
            break;}}

void L_MkFile(void){
  int i,r,c;
  char t[99];
  FILE *arquivo;
  system("clear");
  L_Header();
    printf("\n\t\t    Gerar Relatorio:\n0)Retornar\n1)Ano\n2)Genero\n3)Completo\n\n-> ");
    scanf("%i",&r);
    switch (r) {
      case 0:
      system("clear");
      L_Header();
      L_Menu();
      break;

      case 3:
      arquivo = fopen("Relatorio_Completo.txt","w");
      if(arquivo==NULL){
      	perror("\nErro ao tentar gerar o relatorio!\nEsse erro já foi reportado aos nossos analistas, por favor aguarde nosso contato.");
      	getchar();
      	exit(0);}
      else{
      for(i=0;i<cont;i++){
      fprintf(arquivo, "\nCodigo: %d\nTitulo: %s\nAno: %d\nQuantidade: %d\nGenero: %s\n",(p+i)->codigo,(p+i)->titulo,(p+i)->ano,(p+i)->quantidade,(p+i)->genero);
    }
    printf("\nSeu relatorio foi gerado com sucesso!\n\n");
    fclose(arquivo);
    L_Header();
    L_Menu();}
    break;

    case 2:
    arquivo = fopen("Relatorio_Genero.txt","w");
    if(arquivo==NULL){
      perror("\nErro ao tentar gerar o relatorio!\nEsse erro já foi reportado aos nossos analistas, por favor aguarde nosso contato.");
      getchar();
      exit(0);}
    else{
    system("clear");
    L_Header();
    printf("\nGenero: ");
    scanf(" %s", t);
    for(i=0;i<cont;i++){
      if(strcmp(p[i].genero,t)==0){
        fprintf(arquivo, "\nCodigo: %d\nTitulo: %s\nAno: %d\nQuantidade: %d\nGenero: %s\n",(p+i)->codigo,(p+i)->titulo,(p+i)->ano,(p+i)->quantidade,(p+i)->genero);
      }}
      printf("\nSeu relatorio foi gerado com sucesso!\n\n");
      fclose(arquivo);
      L_Header();
      L_Menu();}
      break;

    case 1:
    arquivo = fopen("Relatorio_Ano.txt","w");
    if(arquivo==NULL){
      perror("\nErro ao tentar gerar o relatorio!\nEsse erro já foi reportado aos nossos analistas, por favor aguarde nosso contato.");
      getchar();
      exit(0);}
    else{
      system("clear");
      L_Header();
      printf("\nAno: ");
      scanf(" %d",&c);
      for(i=0;i<cont;i++){
        if(p[i].ano == c){
          fprintf(arquivo, "\nCodigo: %d\nTitulo: %s\nAno: %d\nQuantidade: %d\nGenero: %s\n",(p+i)->codigo,(p+i)->titulo,(p+i)->ano,(p+i)->quantidade,(p+i)->genero);
          }}
          printf("\nSeu relatorio foi gerado com sucesso!\n\n");
          fclose(arquivo);
          L_Header();
          L_Menu();}
          break;

    default:
    system("clear");
    L_Header();
    L_MkFile();
    break;}}
